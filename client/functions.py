import requests

adress = "http://127.0.0.1:8000"


class fonctions:
    @staticmethod
    def create_deck():
        return requests.get(adress + "/creer-un-deck").json()
    @staticmethod
    def draw_cards(nb_card):
        response = requests.get(adress + "/cartes/{}".format(nb_card)).json()
        #print(response)
        liste=[]
        for card in response['cards']:
            liste.append([card['image'], card['value'], card['suit'], card['code']]) 
        return liste
    @staticmethod
    def count_cards(liste):
        nb_carte= {'HEARTS': 0, 'SPADES': 0, 'DIAMONDS': 0, 'CLUBS': 0}
        for carte in liste:
            nb_carte[carte[2]] += 1
        return nb_carte

 