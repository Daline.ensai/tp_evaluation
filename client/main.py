# Import the modules needed to run the script.
import os
import sys
import requests
from functions import fonctions

# Main definition - constants
menu_actions  = {}  

# =======================
#     MENUS FUNCTIONS
# =======================

# Main menu
def main_menu():
    os.system('clear')
    
    print ("Welcome,\n")
    print ("Veuillez choisir le menu que vous souhaitez lancer:")
    print ("1. Créer un deck")
    print ("2. Tirer des cartes d'un deck")
    #print ("3. Compter les cartes d'une même couleur")
    print ("\n0. Quit")
    choice = input('--> ')
    exec_menu(choice)

    return

# Execute menu
def exec_menu(choice):
    os.system('clear')
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            menu_actions[ch]()
        except KeyError:
            print ("Sélection invalide, veuillez réessayer.\n")
            menu_actions['main_menu']()
    return

# Menu 1
def menu1():
    print ("Création du deck !\n")
    new_deck=fonctions.create_deck()
    print(new_deck)
    print ("Souhaitez-vous faire autre chose?\n")
    print ("Si oui selectionnez l'action à éxecuter sinon choisissez Quit\n")
    print ("2. Tirer des cartes d'un deck")
    #print ("3. Compter les cartes d'une même couleur")
    print ("9. Back")
    print ("0. Quit")
    choice = input('--> ')
    exec_menu(choice)
    return



# Menu 2
def menu2():
    print ("Veuillez entrer le nombre de cartes à tirer s'il vous plait, il doit être compris entre 1 et 52 !\n")
    nb_card = int(input())
    while(nb_card > 52 or nb_card < 1):
        print('')
        print("S'il vous plait, entrer un entier entre 1 et 52 !!!")
        nb_card = int(input())

    if  nb_card==1 :
        print ("Votre carte tirée est la suivante :\n")
    else :
        print ("Vos cartes tirées sont les suivantes :\n")
    global liste    
    liste=fonctions.draw_cards(nb_card)
    j = 0
    for i in liste:
        j += 1
        print('Carte {}:'.format(j), i)
        print('')
    print ("Souhaitez-vous regrouper vos cartes par couleurs?\n")
    print ("Si oui selectionnez l'action 3\n")
    print ("3. Compter les cartes d'une même couleur")
    print ("9. Back")
    print ("0. Quit") 
    choice = input('--> ')
    exec_menu(choice)
    return 

# Menu 3
def menu3():
    print ("Voici la répartiion de vos cartes:\n")
    print('')
    nb_carte=fonctions.count_cards(liste)
    print(nb_carte)

    print ("9. Back")
    print ("0. Quit")
    choice = input('--> ')
    exec_menu(choice)
    return



# Back to main menu
def back():
    menu_actions['main_menu']()

# Exit program
def exit():
    sys.exit()

# =======================
#    MENUS DEFINITIONS
# =======================

# Menu definition
menu_actions = {
    'main_menu': main_menu,
    '1': menu1,
    '2': menu2,
    '3': menu3,
    '9': back,
    '0': exit,
}

# =======================
#      MAIN PROGRAM
# =======================

# Main Program
if __name__ == "__main__":
    # Launch main menu
    main_menu()