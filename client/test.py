from functions import fonctions
import requests
import pytest

def test_count_cards():
    #given
    expected_output={'HEARTS': 3, 'SPADES': 0, 'DIAMONDS': 2, 'CLUBS': 1}
    liste1= [['https://deckofcardsapi.com/static/img/3H.png', '3', 'HEARTS', '3H'], ['https://deckofcardsapi.com/static/img/5D.png', '5', 'DIAMONDS', '5D'], ['https://deckofcardsapi.com/static/img/9H.png', '9', 'HEARTS', '9H'], ['https://deckofcardsapi.com/static/img/4C.png', '4', 'CLUBS', '4C'], ['https://deckofcardsapi.com/static/img/0D.png', '10', 'DIAMONDS', '0D'], ['https://deckofcardsapi.com/static/img/AH.png', 'ACE', 'HEARTS', 'AH']]
    #when
    actual_output= fonctions.count_cards(liste1)
    #then
    assert expected_output==actual_output


if __name__ == "__main__":
    test_count_cards()
    print("Everything passed")
