from functions import fonctions


if __name__ == "__main__":

    print('')
    print("############ Bienvenue sur ce scénario ############")
    print("################ Amusons nous ####################")

    print('')
    print("---------> Création du deck")
    id_deck = fonctions.create_deck()
    print('')
    print(id_deck)

    print('')
    print("---------->Tirage de 10 cartes du deck créé")
    liste = fonctions.draw_cards(10)
    print('')
    print('Les cartes tirées sont :')
    j = 0
    for i in liste:
        j += 1
        print('Card {}:'.format(j), i)
        print('')
    
    print("---------> Regroupement des cartes par couleurs")
    print('')
    print('Résultat :', fonctions.count_cards(liste))
    print('')
