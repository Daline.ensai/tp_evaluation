from fastapi import FastAPI
import requests
import uvicorn


id_deck=''
nb_card_restante=0

app = FastAPI()

@app.get("/")
def read_root():
    return {"https://deckofcardsapi.com/"}



@app.get("/creer-un-deck")
#Fonction permettant de créer un deck
def create_deck():
    global id_deck
    global nb_card_restante
    resp=requests.get("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1")
    id_deck=resp.json()["deck_id"]
    nb_card_restante=resp.json()["remaining"]
    return "l'identifiant du deck est:" + id_deck

#fonction permettant de tirer des cartes d'un deck
@app.get("/cartes/{nb_card}")
def draw_card(nb_card: int):
    global id_deck
    global nb_card_restante

    if id_deck =='':
        response=requests.get("https://deckofcardsapi.com/api/deck/new/draw/?count={}".format(nb_card))
        id_deck=response.json()["deck_id"]
        
    elif (nb_card_restante<nb_card or nb_card>52) :
        response = requests.get("https://deckofcardsapi.com/api/deck/{}/shuffle/".format(id_deck))
        response = requests.get("https://deckofcardsapi.com/api/deck/{}/draw/?count={}".format(id_deck, nb_card))
    else :
        response = requests.get("https://deckofcardsapi.com/api/deck/{}/draw/?count={}".format(id_deck, nb_card))

    response=response.json()
    nb_card_restante=response["remaining"]

    if 'error' in response :
        return {"deck_id": id_deck, "cards":response["cards"], 'error': response["error"]}
    else :
        return {"deck_id":id_deck,"cards":response["cards"]}

    
    if __name__ == "__main__":

         uvicorn.run(app, host="127.0.0.1", port=8000)
   

