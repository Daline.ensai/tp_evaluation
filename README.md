**Examen conception logicielle**

 Ce TP noté repose sur l'utilisation de l'api documentée ici:
[https://deckofcardsapi.com/](url)
Cette API met a disposition des decks de 52 de cartes classiques. Elle propose également de nombreuses opérations détaillées dans la documentation.

Nous avons mis en place dans ce travail :

**Un webservice qui :**

1. Récupère un deck de l'api et renvoie son id : exposé en GET sur /creer-un-deck/

2. Tire des cartes du deck (tirer x cartes du deck parmi les restantes) : exposé en POST sur /cartes/{nombre_cartes} avec un body : {deck_id:str}

Le code du webservice est contenu dans le dossier api, ce dossier contient le main.py pour le lancement du webservice et le requirements.txt pour l'installation des dépendances.



**Un client de votre webservice qui peut:**


1. Initialiser un deck par requête HTTP GET sur /creer-un-deck/, renvoyant l'id en tant que str

2. Tirer des cartes du deck en cours en faisant une requête HTTP POST sur /cartes/{nombre_cartes} avec en body l'id sous forme {deck_id:str}

3. Calculer, pour une liste de cartes donnée en entrée le nombre de cartes de chaque couleur sous forme d'un dictionnaire.
{"H":int,"S":int,"D":int,"C":int} avec H le nombre de coeurs, S le nombre de piques, D le nombre de carreaux, C le nombre de trèfles.
le fichier main.py du dossier client permet de lancer l'application et le test.py permet de tester la fonction de regroupements des cartes par couleurs.

Et enfin un **scénario** qui, en utilisant les fonctions précédentes, initialise un deck, tire 10 cartes et compte le nombre de cartes tirées de chaque couleur. 
Le code du scenario est contenu dans le scenario.py

**Quickstart**

Pour utiliser cette application vous pouvez cloner ce dépot avec la commande: `git clone https://gitlab.com/Daline.ensai/tp_evaluation.git`

**lancement du webservice**

Depuis un terminal se déplacer dans le dossier api et installez les dépendances avec la commande: `pip3 install -r requirements.txt`

Ensuite lancez le serveur à l'aide de la commande: `uvicorn main:app --reload`

Enfin pour requêter le serveur utilisez les commandes suivantes sur un nouveau terminal:
- `wget http://127.0.0.1:8000/creer-un-deck/` pour creer un deck
- `wget http://127.0.0.1:8000/cartes/{nb_carte}` pour tirer nb_carte d'un deck

**lancement du client**

Depuis un terminal se déplacer dans le dossier client et installez les dépendances avec la commande: `pip3 install -r requirements.txt`
Ensuite lancez le client à l'aide de la commande: `python3 main.py` et menez vos actions en suivant le menu.

Pour lancer le scenario utilisez la commande: `python3 scenario.py` 

Et enfin pour le test unitaire lancez la commande: `python3 test.py`


